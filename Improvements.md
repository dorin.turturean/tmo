# What is done well
1. It's separate in modules and easy to test and fast for preview.
2. It's `loadChildren` for used lazy page.
3. Environment it's separated.
4. The state is in the separate module.


# Suggestions
### 1
Routing better to move into a separate module and then to import into the app module.

For example:

- The content file `app-routing.module.ts` 

```typescript
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: '@coding-challenge/stocks/feature-shell#StocksFeatureShellModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

```
And the import into main `app.module.ts`
```typescript
@NgModule({
  declarations: [
    ...
  ],
  imports: [
    AppRoutingModule,
    ...
  ],
  providers: [],
  bootstrap: []
})
export class AppModule {
}
```
### 2
 Need to remove unused imports from components and modules.

### 3
 Use for component change detection on push, because the component will re-render view with last data only when the inputs will be change. And you don't need to call `ChangeDetectorRef` manual run detect changes.

For example:
```typescript
@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartComponent implements OnInit, OnChanges {}
```
### 4
 Mark observable with `$` symbol in the end. For example:  
```typescript
chartData$: BehaviorSubject<Array<Array<string | number>>> = new BehaviorSubject([]);
```
### 5
 Use `async` pipe for send data to the children, because async pipe automatically is unsubscribing when component is destroyed
. For example
```angular2html
<coding-challenge-chart [data]="quotes$ | async"></coding-challenge-chart>
```
### 6
   Use global git `~/.gitconfig` config to put in ignore all visual code config.
### 7
   To change style for component better to use class instead of directly access html tag name
   For example instead of this
   ```css
   mat-form-field {
     display: block;
}
```
use 
```css
.stack-form .stack-form__field {
  display: block;
}
```
### 8
   Use interceptor for handler url to attache all additional parameters
    For example: Token, different headers etc.
### 9
  User the class `HttpParams` to build parameters for method `GET`, `POST`, `DELTE`, `PATCH`, `UPDATE`. 
  For example:
  ```typescript
const httpParams = new HttpParams();
httpParams.set('token', this.env.apiKey);

this.http.get('/api/some-url', {params: httpParams})
```

# Problematic implementations

### 3
 Don't subscribe to the `@Input() data$` to read new values, for this component has hook `OnChanges`
and you can check for new values when input is changed 
```typescript
export class ChartComponent implements OnInit, OnChanges {
  @Input() data;

 ...

    ngOnChanges(changes: SimpleChanges): void {
        const { data } = changes;
    
        if (data) {
          this.chartData = data.currentValue;
        }
      }
}
```
