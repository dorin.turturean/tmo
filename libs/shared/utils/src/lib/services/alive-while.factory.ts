import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

class AliveWhile extends Subject<any> {

  public destroy() {
    this.next();
    this.complete();
  }
}

@Injectable()
export class AliveWhileFactory {

  constructor() {
  }
  get() {
    return new AliveWhile();
  }
}
