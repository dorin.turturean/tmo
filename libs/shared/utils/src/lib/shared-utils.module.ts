import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AliveWhileFactory } from './services/alive-while.factory';

@NgModule({
  imports: [CommonModule],
  providers: [
    AliveWhileFactory,
  ]
})
export class SharedUtilsModule {}
