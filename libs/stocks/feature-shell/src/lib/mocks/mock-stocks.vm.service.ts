import { StocksVmService } from '../services/stocks.vm.service';
import { FormBuilder } from '@angular/forms';

export class MockStocksVmService extends  StocksVmService {
  constructor() {
    super(new FormBuilder());
  }
}
