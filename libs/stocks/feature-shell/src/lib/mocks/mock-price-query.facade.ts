import { of } from 'rxjs';

export class MockPriceQueryFacade {
  selectedSymbol$ = of();
  priceQueries$ = of();


  fetchQuote = jest.fn();
}
