import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule, MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedUiChartModule } from '@coding-challenge/shared/ui/chart';
import { SharedUtilsModule } from '@coding-challenge/shared/utils';

import { StocksComponent } from './stocks/stocks.component';
import { StocksVmService } from './services/stocks.vm.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: StocksComponent }
    ]),
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    SharedUiChartModule,
    SharedUtilsModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  declarations: [StocksComponent],
  providers: [
    StocksVmService
  ]
})
export class StocksFeatureShellModule {
}
