import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { StocksComponent } from './stocks.component';
import { MockStocksVmService } from '../mocks/mock-stocks.vm.service';
import { StocksVmService } from '../services/stocks.vm.service';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedUiChartModule } from '@coding-challenge/shared/ui/chart';
import { MockPriceQueryFacade } from '../mocks/mock-price-query.facade';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AliveWhileFactory } from '../../../../../shared/utils/src/lib/services/alive-while.factory';

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;
  const mockStocksVmService = new MockStocksVmService();
  const priceQueryFacade = new MockPriceQueryFacade();
  const aliveWhileFactory = new AliveWhileFactory();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        SharedUiChartModule,
        MatDatepickerModule,
        MatNativeDateModule,
      ],
      providers: [
        {
          provide: StocksVmService,
          useValue: mockStocksVmService
        },
        {
          provide: PriceQueryFacade,
          useValue: priceQueryFacade,
        },
        {
          provide: AliveWhileFactory,
          useValue: aliveWhileFactory,
        }
      ],
      declarations: [StocksComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
