import { Component, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { StockForm, StocksVmService } from '../services/stocks.vm.service';
import { AliveWhileFactory } from '../../../../../shared/utils/src/lib/services/alive-while.factory';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnDestroy {
  alive$ = this.aliveWhileFactory.get();
  stockPickerForm: FormGroup;
  quotes$ = this.priceQuery.priceQueries$;

  constructor(private stocksVmService: StocksVmService,
              private priceQuery: PriceQueryFacade,
              private aliveWhileFactory: AliveWhileFactory
  ) {
    this.stockPickerForm = this.stocksVmService.create();
    this.priceQuery.getQuery$
      .pipe(
        takeUntil(this.alive$)
      )
      .subscribe(this.updateStockForm.bind(this));

    this.stockPickerForm.statusChanges
      .pipe(
        takeUntil(this.alive$)
      )
      .subscribe(this.formChangeStatus.bind((this)));
  }

  formChangeStatus(status) {
    if (status === 'INVALID' && this.stockPickerForm.hasError('rangeDateIsInvalid')) {
      const { from, to } = this.stockPickerForm.value as StockForm;
      const defaultDate = from || to || new Date();

      this.stockPickerForm.patchValue({ to: defaultDate, from: defaultDate });
    }
  }

  updateStockForm(data: Partial<StockForm> = null) {
    this.stockPickerForm.patchValue(data || {});
  }

  changeSymbol(event: KeyboardEvent) {
    this.fetchQuote();
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, from, to } = this.stockPickerForm.value as StockForm;

      this.priceQuery.fetchQuote(symbol, from, to);
    }
  }

  ngOnDestroy(): void {
    this.alive$.destroy();
  }
}
