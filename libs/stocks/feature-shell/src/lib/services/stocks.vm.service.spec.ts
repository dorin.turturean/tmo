import { FormBuilder } from '@angular/forms';
import { StockForm, StocksVmService } from './stocks.vm.service';

describe('StocksVmService', () => {
  const stocksVmService = new StocksVmService(new FormBuilder());

  it('#create() should be return new form', () => {
    const formGroup = stocksVmService.create();

    expect(formGroup).not.toBeNull();
  });

  it('check default values for stack form', () => {
    const testValue: Partial<StockForm> = { symbol: '23', periodFrom: new Date(), periodTo: new Date()};
    const formGroup = stocksVmService.create(testValue);

    expect(formGroup.value.symbol).toBe(testValue.symbol);
    expect(formGroup.value.periodFrom).toBe(testValue.periodFrom);
    expect(formGroup.value.periodTo).toBe(testValue.periodTo);
  });
});
