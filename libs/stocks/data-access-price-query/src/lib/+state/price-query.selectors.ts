import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PRICEQUERY_FEATURE_KEY, priceQueryAdapter, PriceQueryState } from './price-query.reducer';

const getPriceQueryState = createFeatureSelector<PriceQueryState>(
  PRICEQUERY_FEATURE_KEY
);

export const getSelectedSymbol = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => state.selectedSymbol
);

export const getQuery = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => ({
    from: state.selectedFrom,
    to: state.selectedTo,
    symbol: state.selectedSymbol,
  })
);

const { selectAll } = priceQueryAdapter.getSelectors();

export const getAllPriceQueries = createSelector(
  getPriceQueryState,
  selectAll
);
