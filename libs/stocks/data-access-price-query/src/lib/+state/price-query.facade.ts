import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { FetchPriceQuery } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { getAllPriceQueries, getQuery, getSelectedSymbol } from './price-query.selectors';
import { map, skip } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class PriceQueryFacade {
  selectedSymbol$: Observable<string> = this.store.pipe(select(getSelectedSymbol));
  getQuery$: Observable<{ from: Date, to: Date, symbol: string }> = this.store.pipe(select(getQuery));
  priceQueries$ = this.store.pipe(
    select(getAllPriceQueries),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  constructor(private store: Store<PriceQueryPartialState>) {
  }

  fetchQuote(symbol: string, from: Date, to: Date) {
    this.store.dispatch(new FetchPriceQuery(symbol, from, to));
  }
}
