import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { StocksAppConfig, StocksAppConfigToken } from '@coding-challenge/stocks/data-access-app-config';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import { FetchPriceQuery, PriceQueryActionTypes, PriceQueryFetched, PriceQueryFetchError } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {
        const { to, from, symbol } = action;
        const httpParams = new HttpParams({
          fromObject: {
            token: this.env.apiKey,
            // Todo: need to move function in utils for convert Date into the format YYY.MM.DD
            to: to ? `${to.getFullYear()}.${to.getMonth()}.${to.getDate()}` : null,
            from: from ? `${from.getFullYear()}.${from.getMonth()}.${from.getDate()}` : null
          }
        });

        return this.httpClient
          .get(
            `${this.env.apiURL}/beta/stock/${action.symbol}/chart`,
            {
              params: httpParams
            }
          )
          .pipe(
            map(resp => new PriceQueryFetched(resp as PriceQueryResponse[]))
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );


  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {
  }
}
