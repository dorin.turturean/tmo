import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig = {
  production: true,
  apiKey: 'pk_34af56ab979344108725ba5dd277ae0c',
  apiURL: 'http://localhost:3333/'
};
