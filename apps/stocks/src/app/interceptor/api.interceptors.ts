import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { startsWith } from 'lodash-es';
import { Observable } from 'rxjs';
import { StocksAppConfig, StocksAppConfigToken } from '@coding-challenge/stocks/data-access-app-config';

@Injectable()
export class ApiInterceptorsService implements HttpInterceptor {
  constructor(@Inject(StocksAppConfigToken) private env: StocksAppConfig) {
  }

  apiRequest(req: HttpRequest<any>): HttpRequest<any> {
    const token = this.env.apiKey;
    const headers: any = {
      'Content-Type': 'application/json'
    };
    const httpParams = new HttpParams();
    httpParams.set('token', this.env.apiKey);

    if (token) {
      headers['api-key'] = `${token}`;
    }

    return req.clone({
      url: `${this.env.apiURL}${req.url}`,
      setHeaders: headers
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url } = req;
    const apiRequest = startsWith(url, '/api');

    if (apiRequest) {
      req = this.apiRequest(req);
    }

    return next.handle(req);
  }
}
